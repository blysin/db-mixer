package sqlparser

import (
	"fmt"
	"testing"
)

func testParse(t *testing.T, sql string) {
	stat, err := Parse(sql)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(stat)
}

func TestSet(t *testing.T) {
	sql := "set names gbk"
	testParse(t, sql)
}

func TestSimpleSelect(t *testing.T) {
	//sql := "select last_insert_id() as a"
	sql := "SELECT GROUP_CONCAT(\n               IF(\n                       v.id IS NULL,\n                       IFNULL(\n                               (\n                                   SELECT IF(CURDATE() > vv.endTime, '0', '2')\n                                   FROM t_card vv\n                                   WHERE vv.lotCodeAssist = '2007'\n                                     and vv.lotId = l.id\n                                     AND vv.deleted = 0\n                                   ORDER BY vv.id DESC\n                                   LIMIT 1\n                               ), '0'\n                           ), '1'\n                   )\n           )\nFROM t_card_lot l\n         LEFT JOIN t_card_validtime v ON l.id = v.lotId\n    AND v.deleted = 0\n    AND CURDATE() BETWEEN v.beginTime AND v.endTime\nWHERE l.lotCodeAssist = '2007'\n  -- and l.cardId = c.id"
	testParse(t, sql)
}

func TestMixer(t *testing.T) {
	sql := `admin upnode("node1", "master", "127.0.0.1")`
	testParse(t, sql)

	sql = "show databases"
	testParse(t, sql)

	sql = "show tables from abc"
	testParse(t, sql)

	sql = "show tables from abc like a"
	testParse(t, sql)

	sql = "show tables from abc where a = 1"
	testParse(t, sql)

	sql = "show proxy abc"
	testParse(t, sql)
}
